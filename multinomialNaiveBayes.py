from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_selection import SelectKBest
from bow import *
import numpy as np
# def read_data():
#     datapath = 'negative_polarity'

#     truthful_examples = []
#     deceptive_examples = []

#     truthful_examples_labels = []
#     deceptive_examples_labels = []

#     for root, dirs, files in os.walk(datapath):
#         truth_example = []
#         decept_example = []
#         for filename in files:
#             if("truthful" in root):
#                 truth_example.append(open(root+'/'+filename).read().strip())
#             else:
#                 decept_example.append(open(root+'/'+filename).read().strip())
        
#         if len(truth_example) > 0:
#             truthful_examples.append(truth_example)
#             truthful_examples_labels.append([0] * len(truth_example))
#         if len(decept_example) > 0:
#             deceptive_examples.append(decept_example)
#             deceptive_examples_labels.append([1] * len(decept_example))

#     examples = [truthful_examples[x] + deceptive_examples[x] for x in range(len(truthful_examples))]
#     example_labels = [truthful_examples_labels[x] + deceptive_examples_labels[x] for x in range(len(truthful_examples_labels))]

#     return examples, example_labels

# examples, example_labels = read_data()



# ROUND 1 is deterministic but the other 4 rounds aren't because of MI
def multinomial_naive_bayes(examples, example_labels):
    # Define the hyper parameter
    hyper_parameter = ['all', 200, 100, 50, 25]

    # Cross Validation
    for i in range(len(examples)):
        # Get the training data
        train_data = [item for j in range(len(examples)) if j != i for item in examples[j]]
        train_data_labels = [item for j in range(len(example_labels)) if j != i for item in example_labels[j]]

        train_data, vectorizer = skicit_with_mindf(train_data)

        # print(train_data.head())
        # Feature selection on Mutual information
        kBest = SelectKBest(mutual_info_classif, k=hyper_parameter[i])
        train_data = kBest.fit_transform(train_data.values.tolist(), train_data_labels)
        
        
        test_data = pd.DataFrame(vectorizer.transform(examples[i]).toarray(), columns=vectorizer.get_feature_names_out())
        # print(test_data.head())

        # Get test data
        test_data = np.array(test_data.values.tolist())[:, kBest.get_support(indices=True)]
        test_data_labels = example_labels[i]

        # Train Multinomial Naive Bayes
        mnb = MultinomialNB()
        mnb.fit(train_data, train_data_labels)

        # Predict, using the trained model
        prediction = mnb.predict(test_data)


        print("Prediction with Multinomial Naive Bayes")
        print(f"Cross validation round: {i}, hyper parameter: {hyper_parameter[i]}")
        results(prediction, test_data_labels)
    