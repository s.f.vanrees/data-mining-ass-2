import imp
from bow import *
import numpy as np
from sklearn.linear_model import LogisticRegression
from multinomialNaiveBayes import compare_sets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegressionCV
from sklearn.feature_extraction.text import CountVectorizer

def LogisticRegression1(examples, example_labels):


    hyper_parameter_c = [0.8, 0.9, 1.0, 1.1, 1.2]

    for i in range(len(examples)):
        

        train_data = [item for j in range(len(examples)) if j != i for item in examples[j]]
        train_data_labels = [item for j in range(len(example_labels))if j != i for item in example_labels[j]]


        print('go met de motherfukcing flow babyboy')

        X_train = train_data
        y_train = train_data_labels

        X_test = examples[i]
        y_test = example_labels[i]
      
        X_train, X_test = skicit_LR(X_train, X_test)

        Spam_model = LogisticRegression(solver='liblinear',C= hyper_parameter_c[i], penalty='l1').fit(X_train, y_train)
        pred = Spam_model.predict(X_test)

        accuracy, precision, recall, f1_score, confusion_matrix= compare_sets(pred, y_test)

        print("Prediction with Logistic Regression")
        print(f"accuracy of the model is: {accuracy}")
        print(f"precision of the  model is: {precision}")
        print(f"the hyperparameter for this model was: {hyper_parameter_c[i]}")
        print(f'F1 Score: {f1_score}')
        print(f"recall of the model is: {recall}")
        print(f"Confusion matrix of the model is: {confusion_matrix}")
        print(f"---------")


    return 