import pandas as pd
import re 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics

#Following definitions are used for own BOW code only
#####################################################
def make_bag_of_words(x_text):
    #Remove interpunction and split on words
    clean_x_text = []
    for review in x_text:
        clean_x_text.append(re.sub(r"[^a-zA-Z0-9]", " ", review.lower()).split())

    #Form set of all words
    wordset = make_wordset(clean_x_text)

    #Make for each review a BOW and place them in a BOW list
    BOWlist = []
    for review in clean_x_text:
        BOWlist.append(calculateBOW(wordset,review))

    #Make dataframe from it
    df_bow = pd.DataFrame(BOWlist)

    #Exclude sparse terms
    sparse_BOWlist = make_sparse_BOWlist(BOWlist, df_bow)
    
    return sparse_BOWlist


def calculateBOW(wordset,review):
    tf_diz = dict.fromkeys(wordset,0)
    for word in review:
        tf_diz[word]=review.count(word)
    return tf_diz


def make_wordset(clean_x_text):
    wordlist = []
    for review in clean_x_text:
        for word in review:
            wordlist.append(word)
    #Delete duplicates
    wordset = list(dict.fromkeys(wordlist))
    return wordset


#End of own BOW code
#################################################

#This function is used in both skicit and own BOW code. 
#Delete sparse terms
def make_sparse_BOWlist(BOWlist, df_bow):
    zero_one_sparse_BOWlist = []
    for review in BOWlist:
        review_bow = []
        for word in review:
            if review[word] > 0:
                review_bow.append(1)
            else:
                review_bow.append(0)
        zero_one_sparse_BOWlist.append(review_bow)

    #Sparse_BOWlist is list with only zero's and ones. 
    #Count ones in column, and if in less than 5%, delete column
    df_zeros_ones = pd.DataFrame(zero_one_sparse_BOWlist)

    #Give columns same name
    df_zeros_ones.columns = df_bow.columns

    #Calculate percentage < 0.05
    for column in df_zeros_ones:
        if ((df_zeros_ones[column]==1).sum()) / len(df_zeros_ones)  < 0.05:
            df_bow.drop(column, axis=1, inplace=True)
    return df_bow



#Skicit code
def skicit(xtext, bigram=False):
    if bigram:
        vectorizer = CountVectorizer(stop_words='english',ngram_range=(2,2))
    else:
        vectorizer = CountVectorizer(stop_words='english')
    X = vectorizer.fit_transform(xtext)
    df_bow_sklearn = pd.DataFrame(X.toarray(),columns=vectorizer.get_feature_names())

    #Delete sparse terms
    bow_sklearn = df_bow_sklearn.to_dict('records')
    sparse_BOWlist = make_sparse_BOWlist(bow_sklearn, df_bow_sklearn)

    return sparse_BOWlist

def skicit_LR(xtrain, xtest, bigram=False):

    if bigram:
        count_vector = CountVectorizer(stop_words='english',ngram_range=(2,2))
    else:
        count_vector = CountVectorizer(stop_words='english')
    
    X_train = count_vector.fit_transform(xtrain).toarray()
    X_test = count_vector.transform(xtest).toarray()

    return X_train, X_test

#Skicit code
def skicit_with_mindf(xtext, bigram=False):

    if bigram:
        vectorizer = CountVectorizer(stop_words='english', ngram_range=(2,2), min_df=0.05)
    else:
        vectorizer = CountVectorizer(stop_words='english', min_df=0.05)

    X = vectorizer.fit_transform(xtext)
    df_bow_sklearn = pd.DataFrame(X.toarray(), columns=vectorizer.get_feature_names_out())

    return df_bow_sklearn, vectorizer #sparse_BOWlist



# Get the accuracy, precision and recall
def compare_sets(model_ans, true_ans):

    confusion_matrix = metrics.confusion_matrix(true_ans, model_ans)

    accuracy = (confusion_matrix[0][0] + confusion_matrix[1][1]) / (confusion_matrix[0][0] + confusion_matrix[1][1] + confusion_matrix[0][1] + confusion_matrix[1][0])
    precision = confusion_matrix[0][0] / (confusion_matrix[0][0] + confusion_matrix[0][1])
    recall = confusion_matrix[0][0] / (confusion_matrix[0][0] + confusion_matrix[1][0])
    f1_score = metrics.f1_score(true_ans, model_ans)

    return accuracy, precision, recall, f1_score, confusion_matrix


#Print results
def results(y_pred, y_test):
    accuracy, precision, recall, f1_score, confusion_matrix = compare_sets(y_pred, y_test)
    print(f"accuracy of the model is: {accuracy}")
    print(f"precision of the  model is: {precision}")
    print(f"recall of the model is: {recall}")
    print(f'F1 Score: {f1_score}')
    print(f"Confusion matrix of the model is: {confusion_matrix}")