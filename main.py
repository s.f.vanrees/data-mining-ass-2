#Load data
import os
import pandas as pd
import numpy as np
import collections
import re 
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import tree
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn import decomposition
from bow import *
from logisticRegression import LogisticRegression1
from multinomialNaiveBayes import multinomial_naive_bayes

def read_data():
    datapath = 'negative_polarity'
    truthful_examples=[]
    deceptive_examples=[]

    for root, dirs, files in os.walk(datapath):
        for filename in files:
            if("truthful" in root):
                truthful_examples.append(open(root+'/'+filename).read())
            else:
                deceptive_examples.append(open(root+'/'+filename).read())

    #Split them in folds
    truthful_examples = [s.strip() for s in truthful_examples]
    truthful_folds = [truthful_examples[x:x+80] for x in range(0, len(truthful_examples), 80)]
    
    deceptive_examples = [s.strip() for s in deceptive_examples]
    deceptive_folds = [deceptive_examples[x:x+80] for x in range(0, len(deceptive_examples), 80)]
    
    return truthful_folds, deceptive_folds



def tree_grid_search(x, y):
    x_data = np.array(x)
    y_data = np.array(y)
    pca = decomposition.PCA()
    dec_tree = tree.DecisionTreeClassifier()
    pipe = Pipeline(steps=[('dec_tree', dec_tree)])

    criterion = ['gini', 'entropy']
    max_depth = [2,3,4,5]
    min_samples_split = [2,3,4,5,6,7,8,9,10,]
    min_samples_leaf = [1,2,3,4,5,6,7,8,9,10]
    splitter = ['best', 'random']

    parameters = dict(dec_tree__criterion=criterion,
                      dec_tree__max_depth=max_depth,
                      dec_tree__min_samples_split=min_samples_split,
                      dec_tree__min_samples_leaf=min_samples_leaf,
                      dec_tree__splitter=splitter)

    clf_GS = GridSearchCV(pipe, parameters)
    clf_GS.fit(x_data, y_data)

    print(); print(clf_GS.best_estimator_.get_params()['dec_tree'])

    print(clf_GS.best_score_)

    return 0


#Tree classification
def classification_tree(x_train, x_test, y_train):
    #Apply gridsearch for hyper-parameter optimization
    #hyperparams = tree_grid_search(x_train, y_train)

    
    clf = tree.DecisionTreeClassifier(class_weight=None, criterion='entropy', max_depth=5,
                       max_features=None, max_leaf_nodes=None,
                       min_impurity_decrease=0.0, min_impurity_split=None,
                       min_samples_leaf=10, min_samples_split=2,
                       min_weight_fraction_leaf=0.0, presort=False,
                       random_state=None, splitter='random')
    clf = clf.fit(x_train, y_train)

    y_pred = clf.predict(x_test)
    return y_pred


#Prepare the data
def prepare_data(truthful_folds, deceptive_folds):
    #Make x data and y data appriopiate for tree learning
    x_data = truthful_folds[0:4] + deceptive_folds[0:4] + [truthful_folds[4]] + [deceptive_folds[4]]
    x_flat = [item for sublist in x_data for item in sublist]

    #If you want to use Skicit-learn (second argument is True if you want bigrams. Def = False)
    x_df = skicit(x_flat, False)
    x = x_df.values.tolist()


    #If you want to use Skicit-learn (second argument is True if you want bigrams. Def = False)
    #Example of making x vector of fold 1:
    # x = skicit(truthful_folds[0], False)

    #In case of truthful, we want y-value to be one. Add y-value to dataframe
    # x.insert(288, 'y', 1)

    # print(x.head())
    # return x

    #If you want to use self-written method
    #x = make_bag_of_words(truthful_folds[0])

    x_train = x[0:640]
    x_test = x[640:801]

    y_train = ([1] * 320) + ([0] * 320)
    y_test = ([1] * 80) + ([0] * 80)

    return x_train, x_test, y_train, y_test


def prepare_data_mnb(truthful_folds, deceptive_folds):
    # Multinomial
    examples = [truthful_folds[x] + deceptive_folds[x] for x in range(len(truthful_folds))]
    example_labels = [[0] * len(truthful_folds[x]) + [1] * len(deceptive_folds[x]) for x in range(len(deceptive_folds))]

    return examples, example_labels


def main(classifiers='tree'):
    #Read and prepare data
    truthful_folds, deceptive_folds = read_data()

    if classifiers == 'tree':
        x_train, x_test, y_train, y_test = prepare_data(truthful_folds, deceptive_folds)

        #Tree learner
        y_pred = classification_tree(x_train, x_test, y_train)

        #Print scores
        results(y_pred, y_test)

    elif classifiers == 'mnb':
        ## MULTINOMIAL NAIVE BAYES:
        examples, example_labels = prepare_data_mnb(truthful_folds, deceptive_folds)
        multinomial_naive_bayes(examples, example_labels)

    elif classifiers == 'lr':

        print("-------------------------------------------------")
        examples, example_labels = prepare_data_mnb(truthful_folds, deceptive_folds)
        LogisticRegression1(examples, example_labels)


if __name__ == "__main__":
    main('lr')